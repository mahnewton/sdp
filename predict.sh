#!/bin/bash

while read id; do
    echo $id
    python ./scripts/sdp.py -d ./sample/$id -o ./output  -t s    # s: short distance prediction; l = long distance prediction;
done < 'protein_list.lst'
